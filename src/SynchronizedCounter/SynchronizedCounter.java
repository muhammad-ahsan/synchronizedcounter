package SynchronizedCounter;

/**
 *
 * @author Ahsan
 */
public class SynchronizedCounter {

    // Online
    // http://docs.oracle.com/javase/tutorial/essential/concurrency/sync.html
    // Local
    // ~/tutorial/essential/concurrency/sync.html
    private static int          SynchronizedMethodCtr = 0;
    private static volatile int VolatileCtr           = 0;
    private static long         IntrinsicLockCtr1     = 0;
    private static long         IntrinsicLockCtr2     = 0;
    private static final Object lock1                 = new Object();
    private static final Object lock2                 = new Object();

    public static void IntrinsicLock_Increment1() {
        /* Exclusively acquiring lock1 and no other thread can execute 
           same method IntrinsicLock_Increment1() to cause interference */
        
//        Object level locking. 
//        synchronized (this) {
//            IntrinsicLockCtr1++;
//        }
//        Class level locking. 
//        synchronized (SynchronizedCounter.class) {
//            IntrinsicLockCtr1++;
//        }
        
//        OR
        
        synchronized (lock1) {
            IntrinsicLockCtr1++;
        }
    }

    public static void IncrementIntrinsicLockCtr1() {

        /* Exclusively acquiring lock1 and no other thread can execute 
           same method IntrinsicLock_Increment1() to cause interference */
        synchronized (lock2) {
            IntrinsicLockCtr2++;
        }
    }

    // Synchronized method locks all object or class (static)
    public synchronized static void IncrementSynchronizedMethodCtr() {
        SynchronizedMethodCtr++;
    }
    // Synchronized method locks all object or class (static)
    public synchronized static void DecrementSynchronizedMethodCtr() {
        SynchronizedMethodCtr--;
    }

    public static void IncrementVolatileCtr(){
        VolatileCtr++;
    }
    public static int getSynchronizedMethodCtr() {
        return SynchronizedMethodCtr;
    }
    public static long getIntrinsicLockCtr1() {
        return IntrinsicLockCtr1;
    }
    public static int getVolatileCtr() {
        return VolatileCtr;
    }
}