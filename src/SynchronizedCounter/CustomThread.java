package SynchronizedCounter;

/**
 *
 * @author Ahsan
 */
public class CustomThread extends Thread {
    @Override
    public void run() {
        System.out.println("Custom thread start");

        for (int i = 0; i < 10; i++) {
            SynchronizedCounter.IncrementSynchronizedMethodCtr();
            SynchronizedCounter.IntrinsicLock_Increment1();
            SynchronizedCounter.IncrementVolatileCtr();
        }
        System.out.println("Custom thread exit");
    }
}
