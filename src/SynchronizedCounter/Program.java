package SynchronizedCounter;

/**
 *
 * @author MAhsan
 */
public class Program {
    public static void main(String[] args) throws InterruptedException {
        System.out.println("Main thread started");

        CustomThread newthread = new CustomThread();

        newthread.start();

        for (int i = 0; i < 3; i++) {
            SynchronizedCounter.DecrementSynchronizedMethodCtr();
            SynchronizedCounter.IntrinsicLock_Increment1();
            SynchronizedCounter.IncrementVolatileCtr();
        }

        
        newthread.join();
        // Every other thread processing finished until here
        
        System.out.println("Main exit");
        System.out.println("Counter using synchronized method  = " + SynchronizedCounter.getSynchronizedMethodCtr());
        System.out.println("Counter using intrinsic Lock = " + SynchronizedCounter.getIntrinsicLockCtr1());
        System.out.println("Counter using volatile property "+ SynchronizedCounter.getVolatileCtr());
    }
}
